package main

import (
  "fmt"
  "os"
  "bufio"
  "strings"
  "sort"
)

func main() {

  // Declare variables 
  lines := 0 // for question 1: how many lines are in the file?
  mids := map[string]int{} // for questions 2 and 3: how many unique merchant-id’s are in the file? and how many pageviews did each of the top 5 mids receive?
  pageviews := map[string]int{} // for question 4: What are the top 20 product-id’s by page view counts and how many pageviews did each of them receive?
  /*
   * for question 5: for each of the top 5 mids, list the top ten product-id’s by page views and how many page views each of them received. 
   * (sort of cheating by hard-coding the mids, so I don't have to loop through the data twice)
   */
  pids := map[string]map[string]int { 
    "400": map[string]int{},
    "154": map[string]int{},
    "911": map[string]int{},
    "1102": map[string]int{},
    "830": map[string]int{},
  }
  file, _ := os.Open("DEC40C4074D011E28DF96202B5E5F0B3")
  scanner := bufio.NewScanner(file)

  // Populate variables for Questions 1, 2, 3, 4, and 5
  for scanner.Scan() {
    // Question 1
    lines++

    // Questions 2 and 3
    mid := strings.Split(scanner.Text(), "|")[1]
    if mids[mid] != 0 {
      mids[mid]++
    } else {
      mids[mid] = 1
    }

    // Question 4
    pid := strings.Split(scanner.Text(), "|")[2]
    if pid != "-" && pid != "" {
      if pageviews[pid] != 0 {
        pageviews[pid]++
      } else {
        pageviews[pid] = 1
      }
    }

    // Question 5
    if _, ok := pids[mid]; ok {
      if pids[mid][pid] != 0 {
        pids[mid][pid] += 1
      } else {
        pids[mid][pid] = 1
      }
    }
  }

  // Print out answers
  // Question 1
  fmt.Println("There are", lines, "lines in the file.\n")

  // Question 2
  fmt.Println("There are", len(mids), "unique mids in the file.\n")

  // Question 3
  // Not in order because maps... also can't figure out how to arrange by value
  for key, value := range topN(mids, 5) {
    fmt.Println("MID:", key, "Pageviews:", value)
  }
  fmt.Println()

  // Question 4
  for key, value := range topN(pageviews, 20) {
    fmt.Println("PID:", key, "Pageviews:", value)
  }
  fmt.Println()

  // Question 5
  for key := range pids {
    fmt.Println("MID:", key)
    for key, value := range topN(pids[key], 10) {
      fmt.Println("PID:", key, "Pageviews:", value)
    }
    fmt.Println()
  }
  fmt.Println()
}

// iterate over a map and return a new map containing only the key-value pairs with the highest values
func topN(m map[string]int, i int) map[string]int {
  if len(m) < i {
    i = len(m)
  }
  top := map[string]int{}
  var values []int
  for k := range m {
    values = append(values, m[k])
  }

  sort.Sort(sort.Reverse(sort.IntSlice(values)))
  values = values[:i]

  for k := range m {
    for v := range values {
      if m[k] == values[v] {
        top[k] = values[v]
      }
    }
  }

  return top
}

