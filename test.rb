#!/usr/bin/ruby

# Declare variables
lines = 0 # for question 1: how many lines are in the file?
mids = {} # for question 2: how many unique merchant-id’s are in the file?
pids = { "400" => {}, "154" => {}, "911" => {}, "1102" => {}, "830" => {}} # for question 5: for each of the top 5 mids, list the top ten product-id’s by page views and how many page views each of them received. (sort of cheating by hard-coding the mids, so I don't have to loop through the data twice)
seconds = {} # for questions 6 and 7: what are the top 20 seconds with the most/least requests in that second?
data = IO.readlines("DEC40C4074D011E28DF96202B5E5F0B3")

# Question 1
lines = data.length
puts "There are #{lines} lines in the file.\n"

# Populate variables for Questions 2, 5, 6, and 7
data.each { |x|
  mid = x.split("|")[1]
  pid = x.split("|")[2]
  second = x.split("|")[5]

  mids.key?(mid) ? mids[mid] += 1 : mids[mid] = 1
  if pids.key?(mid)
    pids[mid].key?(pid) ? pids[mid][pid] += 1 : pids[mid][pid] = 1
  end
  seconds.key?(second) ? seconds[second] += 1 : seconds[second] = 1
}

# Question 2
puts "There are #{mids.keys.length} unique mids in the file.\n"

# Question 5
pids.keys.each { |x|
  # sort pids by value
  sorted_pids = pids[x].sort_by {|_key, value| value}

  # print top 10
  puts "The top 10 pids for client #{x} are", sorted_pids.first(10) # prints the key as well, which I don't want, but also don't feel like fixing right now #yolo
  puts "\n"
}

# Question 6
sorted_seconds = seconds.sort_by {|_key, value| value}
puts "The top 20 seconds with the most requests are", sorted_seconds.first(20)
puts "\n"

# Question 7
puts "The top 20 seconds with the least requests are", sorted_seconds.reverse.first(20)
