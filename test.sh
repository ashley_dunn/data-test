#!/bin/bash

# Declare variables
declare -A browsers # for question 9
declare -A mid400   # for question 10
declare -A mid154   # for question 10
declare -A mid911   # for question 10
declare -A mid1102  # for question 10
declare -A mid830   # for question 10
IFS="|"
filename="$1"
linenum=1

# Populate variables
while read -r line
do
  read -a array <<< "$line" # split line on '|' into an array
  mid="${array[1]}"
  browser="${array[6]}"

  # Question 9
  if [ ${browsers[$browser]+_} ]; then ((browsers[$browser]++)); else browsers[$browser]=1; fi 

  # Question 10
  #if [ "$mid" = "400" ]
  #then
  #  if [ ${mid400[browser]+_} ]; then mid400[browser] += 1; else mid400[browser] = 1; fi
  #fi

  #echo "$linenum"
  #linenum=$[$linenum +1]

done < "$filename"

# Print answers
echo "FINISHED"
#sort -rn -browsers2
#echo ${mid400[0]}
